comienzo:
CLEAR
ON ERROR GOTO errores
REDIM Cuadricula(0 TO 62, 0 TO 46)
SCREEN 12
CLS
COLOR 9
LOCATE 1, 35
PRINT "U-N-I 3.0"
COLOR 11
PRINT
PRINT "Lupa"
PRINT "Versi�n: 2.0"
COLOR 15
LOCATE 24, 54
PRINT "De Gabriel Gioiosa Farina."
SLEEP 3
CLS

'valores por defecto:
col% = 0
fil% = 0
colo% = 11
LINE (0, 0)-(10, 8), 15
LINE (0, 9)-(0, 8), 15
LINE (0, 0)-(10, 8), 15, B
DIM co%(100)
GET (0, 0)-(10, 8), co%
PUT (0, 0), co%
LINE (0, 0)-(10, 10), 15, BF
DIM cur%(100)
GET (0, 0)-(10, 10), cur%
GOSUB cuadri
WHILE in$ <> CHR$(27)
  PUT (col% * 10, fil% * 10), cur%, XOR
  PUT (colo% * 10, 471), co%
10 in$ = INKEY$: IF in$ = "" THEN GOTO 10
  PUT (colo% * 10, 471), co%
  PUT (col% * 10, fil% * 10), cur%, XOR
  IF in$ = "7" THEN IF col% > 0 THEN col% = col% - 1: IF fil% > 0 THEN fil% = fil% - 1
  IF in$ = "1" THEN IF col% > 0 THEN col% = col% - 1: IF fil% < 46 THEN fil% = fil% + 1
  IF in$ = "9" THEN IF col% < 62 THEN col% = col% + 1: IF fil% > 0 THEN fil% = fil% - 1
  IF in$ = "3" THEN IF col% < 62 THEN col% = col% + 1: IF fil% < 46 THEN fil% = fil% + 1
  IF in$ = "4" OR in$ = CHR$(0) + "K" THEN IF col% > 0 THEN col% = col% - 1
  IF in$ = "6" OR in$ = CHR$(0) + "M" THEN IF col% < 62 THEN col% = col% + 1
  IF in$ = "8" OR in$ = CHR$(0) + "H" THEN IF fil% > 0 THEN fil% = fil% - 1
  IF in$ = "2" OR in$ = CHR$(0) + "P" THEN IF fil% < 46 THEN fil% = fil% + 1
  IF in$ = CHR$(13) OR siem% = 1 THEN
    Cuadricula(col%, fil%) = colo%
    LINE ((col% * 10) + 1, (fil% * 10) + 1)-((col% * 10) + 9, (fil% * 10) + 9), colo%, BF
  END IF
  IF in$ = "s" OR in$ = "S" THEN GOSUB siempre
  IF in$ = "g" OR in$ = "G" THEN GOSUB grabar
  IF in$ = "l" OR in$ = "L" THEN GOSUB leer
  IF in$ = "o" OR in$ = "O" THEN GOTO comienzo
  IF in$ = "b" OR in$ = "B" THEN GOSUB borrar
  IF in$ = "a" OR in$ = "A" THEN GOSUB ayuda
  IF in$ = CHR$(8) THEN GOSUB borrararchivo
  IF in$ = "d" OR in$ = "D" THEN GOSUB listar
  IF in$ = "v" OR in$ = "V" THEN GOSUB ver
  LINE (631, 471)-(639, 479), colo%, BF
  IF in$ = "c" OR in$ = "C" THEN
    IF colo% < 15 THEN colo% = colo% + 1 ELSE colo% = 0
  END IF
WEND
END

grabar:
CLS
GOSUB archivo
IF archi$ <> ".LUP" THEN
  OPEN archi$ FOR OUTPUT AS #1
  LOCATE 1, 69
  COLOR 10
  PRINT "Grabando..."
  COLOR 15
  FOR col% = 0 TO 46
    FOR fil% = 0 TO 62
      PRINT #1, USING "##"; Cuadricula(fil%, col%);
    NEXT fil%
  NEXT col%
  CLOSE #1
END IF
GOSUB cuadri
GOSUB actualiza
RETURN

leer:
GOSUB archivo
IF archi$ <> ".LUP" THEN
  a = 1
  FILES archi$
  OPEN archi$ FOR BINARY AS #1
  LOCATE 1, 70
  COLOR 10
  PRINT "Leyendo..."
  FOR col% = 0 TO 46
    FOR fil% = 0 TO 62
      Cuadricula(fil%, col%) = VAL(INPUT$(2, 1))
    NEXT fil%
  NEXT col%
  CLOSE #1
END IF
sig:
GOSUB ver
RETURN

siempre:
IF siem% = 1 THEN siem% = 0: RETURN
IF siem% = 0 THEN siem% = 1: RETURN

archivo:
CLS
INPUT "Archivo: "; archi$
IF INSTR(1, archi$, ".") = 0 THEN archi$ = archi$ + ".LUP"
RETURN

borrar:
IF BORR% = 1 THEN BORR% = 0: colo% = coloant%: RETURN
IF BORR% = 0 THEN BORR% = 1: coloant% = colo%: colo% = 0: RETURN

ayuda:
CLS
COLOR 14
PRINT "IZQUIERDA= flecha izquierda/numero 4"
PRINT "DERECHA= flecha derecha/numero 6"
PRINT "ARRIBA= flecha arriba/numero 8"
PRINT "ABAJO= flecha abajo/numero 2"
PRINT "DIAGONAL= teclas 1-3-7-9"
PRINT "LAPIZ= tecla S"
PRINT "PINTAR CUADRADITO= tecla ENTER"
PRINT "GOMA= tecla B"
PRINT "COLORES= tecla C"
PRINT "OTRO DIBUJO= tecla O"
PRINT "LEER ARCHIVO= tecla L"
PRINT "GRABAR DIBUJO= tecla G"
PRINT "LISTAR= tecla D"
PRINT "BORRAR ARCHIVO= tecla BACKSPACE"
PRINT
PRINT
PRINT "                                    Presione cualquier tecla para continuar."
pau$ = INPUT$(1)
GOSUB cuadri
GOSUB actualiza
RETURN

borrararchivo:
GOSUB archivo
IF archi$ <> ".LUP" THEN
  LOCATE 1, 69
  COLOR 12
  PRINT "Borrando..."
  COLOR 15
  KILL archi$
END IF
GOSUB cuadri
GOSUB actualiza
RETURN

listar:
CLS
INPUT "DIRECTORIO: "; directorio$
directorio$ = directorio$ + "*.LUP"
FILES directorio$
pau$ = INPUT$(1)
CLS
GOSUB cuadri
GOSUB actualiza
RETURN

ver:
CLS
INPUT "Tama�o: ", tam%
CLS
coli% = 46 * tam%
fili% = 62 * tam%
FOR col% = 0 TO coli% STEP tam%
  FOR fil% = 0 TO fili% STEP tam%
    LINE (fil%, col%)-(fil% + tam% - 1, col% + tam% - 1), Cuadricula(fil% / tam%, col% / tam%), BF
  NEXT fil%
NEXT col%
GOSUB Listo
pau$ = INPUT$(1)
GOSUB cuadri
GOSUB actualiza
RETURN

actualiza:
FOR col% = 0 TO 460 STEP 10
  FOR fil% = 0 TO 620 STEP 10
    LINE (fil% + 1, col% + 1)-(fil% + 9, col% + 9), Cuadricula(fil% / 10, col% / 10), BF
  NEXT fil%
NEXT col%
col% = 0
fil% = 0
GOSUB Listo
RETURN
cuadri:
CLS
FOR a% = 0 TO 630 STEP 10: LINE (a%, 0)-(a%, 470), 7: NEXT a%
FOR a% = 0 TO 470 STEP 10: LINE (0, a%)-(630, a%), 7: NEXT a%
FOR x = 10 TO 160 STEP 10: LINE (x - 10, 471)-(x, 479), (x - 10) / 10, BF: NEXT x
colo% = 11
RETURN

Listo: SOUND 100, 1: SOUND 500, 1: SOUND 1000, 1: RETURN

errores:
IF ERR = 53 AND a = 1 THEN
  PRINT "El archivo no existe."
  pau$ = INPUT$(1)
  a = 0
  GOSUB cuadri
  RESUME actualiza
END IF
IF ERR = 53 THEN PRINT "No se ha encontrado ningun archivo.": RESUME NEXT
END

