' Lee archivos en formato IMG de U-N-I.
DECLARE SUB LeeIMG ()
SCREEN 13
LeeIMG
pau$ = INPUT$(1)
SCREEN 0

' Lee una im�gen en modo gr�fico 13.
SUB LeeIMG
  DEF SEG = &HA000
  OPEN COMMAND$ + ".IMG" FOR BINARY AS #2
  IF INPUT$(1, 2) = "�" THEN
    CLOSE #2
    BLOAD COMMAND$ + ".img", 0
    EXIT SUB
  END IF
  SEEK 2, 1
  x1% = VAL(INPUT$(3, 2))
  x2% = VAL(INPUT$(3, 2))
  y1% = VAL(INPUT$(3, 2))
  y2% = VAL(INPUT$(3, 2))
  IF x1% = 0 AND x2% = 0 AND y1% = 0 AND y2% = 0 THEN x2% = 320: y2% = 200
  FOR y = x1% TO x2%
    FOR x = y1% TO y2%
      POKE ((320 * x) + y), ASC(INPUT$(1, 2))
    NEXT x
  NEXT y
  CLOSE #2
END SUB

