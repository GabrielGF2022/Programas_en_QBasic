DECLARE SUB GetDisk (dv%)
DECLARE SUB GetPath (dv%, Path$)
CLS
IF COMMAND$ = "" OR COMMAND$ = "/?" OR COMMAND$ = "/H" OR COMMAND$ = "/A" THEN GOTO Ayuda
GetDisk dv%
GetPath dv%, Path$
IF dv% = 0 THEN Drive$ = "A:"
IF dv% = 1 THEN Drive$ = "A:"
IF dv% = 2 THEN Drive$ = "B:"
IF dv% = 3 THEN Drive$ = "C:"
IF dv% = 4 THEN Drive$ = "D:"
IF Path$ <> "\" THEN
  Lugar$ = Drive$ + LEFT$(Path$, LEN(Path$) - 1)
ELSE
  Lugar$ = Drive$ + "\"
END IF
ON ERROR GOTO Crear
VIEW PRINT 1 TO 1
FILES "C:\DOS\TEMP\P.TMP"
VIEW PRINT
Listo:
DIM dir$(0 TO 20)
OPEN "C:\DOS\TEMP\P.TMP" FOR INPUT AS #1
  WHILE EOF(1) = 0
    in$ = INPUT$(1, 1)
    IF in$ <> CHR$(13) THEN dir$(x%) = dir$(x%) + in$ ELSE x% = x% + 1: SEEK 1, SEEK(1) + 1
  WEND
CLOSE #1
FOR y% = 0 TO x% - 1
  dir$(y%) = LEFT$(dir$(y%), LEN(dir$(y%)) - 11)
NEXT y%
FOR y% = 0 TO x% - 1
  FOR num% = 1 TO 50
    arch$ = dir$(y%) + RIGHT$(STR$(num%), LEN(STR$(num%)) - 1) + ".BAT"
    OPEN arch$ FOR INPUT AS #2
      IF LOF(2) <> 0 THEN SEEK #2, 5: nom$ = INPUT$(8, 2)
    CLOSE #2
    IF INSTR(1, UCASE$(nom$), COMMAND$) <> 0 THEN EXIT FOR
  NEXT num%
  IF INSTR(1, UCASE$(nom$), COMMAND$) <> 0 THEN EXIT FOR
NEXT y%
SHELL arch$
CHDIR Lugar$
IF CSRLIN > 2 THEN LOCATE CSRLIN - 2
SYSTEM

Crear:
 IF ERR = 53 THEN
   shel$ = "DIR /S /B C:\DIRECTS.MNU > C:\DOS\TEMP\P.TMP"
   SHELL shel$
   RESUME Listo
 END IF
 PRINT "ERROR: "; ERR
END

Ayuda:
PRINT "Ejecutar 1.0  de PROGS 3.0"
PRINT "  Gabriel Gioiosa Farina"
PRINT
PRINT " Con este programa se puede ejecutar un programa que este en el menu de disco de"
PRINT "PROGS, sin entrar a este, con la siguiente sintaxis:"
PRINT
PRINT "P programa_a_ejecutar"
PRINT
PRINT " Programa_a_ejecutar  es el nombre tal como  aparece en el menu de PROGS. No  es"
PRINT "necesario escribir el nombre completo."
PRINT
PRINT " El programa creara un archivo temporario en el directorio C:\DOS\TEMP. Para que"
PRINT "la busqueda se realize mas rapidamente conviene no borrarlo. Igualmente el  pro-"
PRINT "grama lo volvera a crear."
PRINT
SYSTEM

