DECLARE SUB Paleta (c$, colore%, colo%(), mp%)
DECLARE SUB Funcion (a, b, c, pas, g%, colo%)
mp% = 13
SCREEN mp%
REDIM colo%(0 TO 255, 1 TO 3)
Paleta "L", colore%, colo%(), mp%

' Funci�n de 1� grado.
colo% = 15
FOR x = 94 TO -94 STEP -1
  a = x / 10
  colo% = colo% + 1
  Funcion a, 0, 0, .01, 1, colo%
NEXT x
Pau$ = INPUT$(1)
CLS
' Funci�n de 2� grado.
colo% = 15
FOR x = 94 TO -94 STEP -1
  a = x / 1000
  colo% = colo% + 1
  Funcion a, 0, 0, .01, 2, colo%
NEXT x
Pau$ = INPUT$(1)
CLS
' Funci�n de 3� grado.
colo% = 15
FOR x = 94 TO -94 STEP -1
  a = x / 10000
  colo% = colo% + 1
  Funcion a, 0, 0, .01, 3, colo%
NEXT x
SLEEP

SUB Funcion (a, b, c, pas, g%, colo%)
  FOR x = -10 TO 10 STEP pas
    y = a * ((x + c) ^ g%) + b
    PSET (160 + (x * 10), 100 - (y * 10)), colo%
  NEXT x
END SUB

' Trabaja con la paleta.
' c$ "L"= Lee una paleta grabada.
' c$ "G"= Graba la paleta.
' c$ "R"= Restaura los 15 colores principales de la paleta.
SUB Paleta (c$, colore%, colo%(), mp%)
  c$ = UCASE$(c$)
  IF mp% = 13 THEN cantcolor = 255
  IF mp% = 12 THEN cantcolor = 15
  IF c$ = "L" THEN
    REDIM colo%(0 TO 255, 1 TO 3)
    OPEN "\progs\cnf\paleta" + RIGHT$(STR$(mp%), 2) + ".pal" FOR BINARY AS #1
    FOR colo% = 0 TO cantcolor
      FOR tubo = 1 TO 3
        colo%(colo%, tubo) = VAL(INPUT$(2, 1))
      NEXT tubo
      PALETTE colo%, (65536 * colo%(colo%, 3)) + (256 * colo%(colo%, 2)) + colo%(colo%, 1)
    NEXT colo%
    CLOSE #1
  END IF
  IF c$ = "G" THEN
    OPEN "\progs\cnf\paleta" + RIGHT$(STR$(mp%), 2) + ".pal" FOR OUTPUT AS #1
    FOR colo% = 0 TO cantcolor
      FOR tubo = 1 TO 3
        PRINT #1, RIGHT$(STR$(colo%(colo%, tubo)), 2);
      NEXT tubo
    NEXT colo%
    CLOSE #1
  END IF
  IF c$ = "R" THEN IF colore% > -1 AND colore% < 16 THEN ON colore% + 1 GOSUB 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ELSE GOSUB 16
  IF c$ = "N" THEN
    FOR x = 0 TO 17
      ON x GOSUB 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
    NEXT x
  END IF
EXIT SUB
0   'Negro
  colo%(0, 1) = 0
  colo%(0, 2) = 0
  colo%(0, 3) = 0
  RETURN
1   'Azul Claro
  colo%(1, 1) = 0
  colo%(1, 2) = 0
  colo%(1, 3) = 32
  RETURN
2   'Verde Claro
  colo%(2, 1) = 0
  colo%(2, 2) = 32
  colo%(2, 3) = 0
  RETURN
3   'Celeste Claro
  colo%(3, 1) = 0
  colo%(3, 2) = 32
  colo%(3, 3) = 32
  RETURN
4   'Rojo Claro
  colo%(4, 1) = 32
  colo%(4, 2) = 0
  colo%(4, 3) = 0
  RETURN
5   'Roza Claro
  colo%(5, 1) = 32
  colo%(5, 2) = 0
  colo%(5, 3) = 32
  RETURN
6   'Amarillo Claro
  colo%(6, 1) = 32
  colo%(6, 2) = 32
  colo%(6, 3) = 0
  RETURN
7   'Griz
  colo%(7, 1) = 32
  colo%(7, 2) = 32
  colo%(7, 3) = 32
  RETURN
8   'Negro Claro
  colo%(8, 1) = 16
  colo%(8, 2) = 16
  colo%(8, 3) = 16
  RETURN
9   'Azul
  colo%(9, 1) = 0
  colo%(9, 2) = 0
  colo%(9, 3) = 63
  RETURN
10   'Verde
  colo%(10, 1) = 0
  colo%(10, 2) = 63
  colo%(10, 3) = 0
  RETURN
11   'Celeste
  colo%(11, 1) = 0
  colo%(11, 2) = 63
  colo%(11, 3) = 63
  RETURN
12   'Rojo
  colo%(12, 1) = 63
  colo%(12, 2) = 0
  colo%(12, 3) = 0
  RETURN
13   'Roza
  colo%(13, 1) = 63
  colo%(13, 2) = 0
  colo%(13, 3) = 63
14   'Amarillo
  colo%(14, 1) = 63
  colo%(14, 2) = 63
  colo%(14, 3) = 0
  RETURN
15   'Blanco
  colo%(15, 1) = 63
  colo%(15, 2) = 63
  colo%(15, 3) = 63
  RETURN
16 FOR colo% = 16 TO 78
     colo%(colo%, 1) = 0
     colo%(colo%, 2) = 0
     colo%(colo%, 3) = colo% - 15
  NEXT colo%
  FOR colo% = 79 TO 141
     colo%(colo%, 1) = 0
     colo%(colo%, 2) = colo% - 78
     colo%(colo%, 3) = 0
  NEXT colo%
  FOR colo% = 142 TO 204
     colo%(colo%, 1) = colo% - 141
     colo%(colo%, 2) = 0
     colo%(colo%, 3) = 0
  NEXT colo%
  FOR colo% = 0 TO cantcolor
    PALETTE colo%, (65536 * colo%(colo%, 3)) + (256 * colo%(colo%, 2)) + colo%(colo%, 1)
  NEXT colo%
  RETURN
END SUB

